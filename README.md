# Exoplanet metrics Service

Application to explore specific metrics out of exoplanet data list.

## Run command

```bash
mvn spring-boot:run -Dspring-boot.run.profiles=dev
```

## Usage

Import curl command in postman or run it from cmd.

```bash
curl --location 'http://localhost:8080/v1/exoplanet/metrics'
```

## Sample output

```json
{
    "noOfOrphanPlanets": 2,
    "planetOrbitingHottestStar": "V391 Peg b",
    "groupByYear": {
        "1781": {
            "small": [
                {
                    "TypeFlag": 0,
                    "PlanetIdentifier": "Uranus",
                    "RadiusJpt": 0.362775,
                    "DiscoveryYear": "1781",
                    "HostStarTempK": 5778
                }
            ],
            "medium": [
                {
                    "TypeFlag": 0,
                    "PlanetIdentifier": "Uranus2",
                    "RadiusJpt": 1.362775,
                    "DiscoveryYear": "1781",
                    "HostStarTempK": 5778
                }
            ],
            "large": [
                {
                    "TypeFlag": 0,
                    "PlanetIdentifier": "Uranus1",
                    "RadiusJpt": 2.362775,
                    "DiscoveryYear": "1781",
                    "HostStarTempK": 5778
                }
            ]
        },
        "1846": {
            "small": [
                {
                    "TypeFlag": 0,
                    "PlanetIdentifier": "Neptune",
                    "RadiusJpt": 0.35221925,
                    "DiscoveryYear": "1846",
                    "HostStarTempK": 5778
                }
            ]
        }
    }
}
```
