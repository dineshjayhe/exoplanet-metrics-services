package com.gem.exoplanent;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.core.env.Environment;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@AutoConfigureWireMock(port = 6010, stubs = "classpath:/wiremock/stubs", files = "classpath:/wiremock")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
class ExoplanentMetricsServicesApplicationTests {

	@Autowired
	Environment environment;

	@Test
	void exoplanetMetrics_expectSuccess() {
		initRequestSpec();
		given().log().all()
				.when()
				.get("/v1/exoplanet/metrics")
				.then()
				.log()
				.all().and()
				.statusCode(200)
				.body("noOfOrphanPlanets", equalTo(2))
				.body("planetOrbitingHottestStar", equalTo("Kepler"))
				.body("groupByYear.2010", notNullValue());
	}

	void initRequestSpec() {
		RestAssured.reset();
		environment.getProperty("local.server.port");
		String url = "http://localhost:" + environment.getProperty("local.server.port");
		RestAssured.requestSpecification = new RequestSpecBuilder()
				.setConfig(RestAssured.config())
				.setContentType(ContentType.JSON)
				.setBaseUri(url).build();
	}


}
