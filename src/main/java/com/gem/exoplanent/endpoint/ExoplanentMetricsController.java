package com.gem.exoplanent.endpoint;

import com.gem.exoplanent.modal.Response;
import com.gem.exoplanent.service.ExoplanetService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class ExoplanentMetricsController {

    private final ExoplanetService exoplanetService;

    @RequestMapping("v1/exoplanet/metrics")
    public Response hello() {
        return exoplanetService.getMetricsOfExoplanet();
    }
}
