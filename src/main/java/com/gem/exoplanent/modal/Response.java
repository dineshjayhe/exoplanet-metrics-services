package com.gem.exoplanent.modal;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class Response {
    private long noOfOrphanPlanets;
    private String planetOrbitingHottestStar;
    private Map<String, Map<String, List<ExoplanetItem>>> groupByYear;
}
