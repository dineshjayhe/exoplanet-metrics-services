package com.gem.exoplanent.modal;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ExoplanetItem {

    @JsonProperty("TypeFlag")
    private Integer typeFlag;

    @JsonProperty("PlanetIdentifier")
    private String planetIdentifier;

    @JsonProperty("RadiusJpt")
    private Float radiusJpt;

    @JsonProperty("DiscoveryYear")
    private String discoveryYear;

    @JsonProperty("HostStarTempK")
    private Integer hostStarTempK;
}
