package com.gem.exoplanent.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gem.exoplanent.modal.ExoplanetItem;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.util.List;

@Service
@Slf4j
public class ExoplanetClient {

    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private ObjectMapper objectMapper;

    @Value("${com.gem.exoplanet.endpoint}")
    private String endpoint;

    @Value("${com.gem.exoplanet.path}")
    private String path;

    private String url;

    @PostConstruct
    public void initialize() {
        url = endpoint + path;
    }

    public List<ExoplanetItem> getExoplanetData() {
        log.info("Calling exoplanet API");
        HttpEntity<Void> httpEntity = new HttpEntity<>(null, getHttpHeaders());
        String textPlainResponse = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class).getBody();
        List<ExoplanetItem> response = null;
        try {
            response = objectMapper.readValue(textPlainResponse, new TypeReference<List<ExoplanetItem>>() {});
            log.info("Received response");
        } catch (JsonProcessingException e) {
            log.error("Could not parse String to Java Object: ", e);
            throw new RuntimeException(e);
        }
        return response;
    }

    private HttpHeaders getHttpHeaders() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_PLAIN_VALUE); //Since its content type is text
        return httpHeaders;
    }
}
