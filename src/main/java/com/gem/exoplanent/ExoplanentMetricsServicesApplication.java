package com.gem.exoplanent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExoplanentMetricsServicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExoplanentMetricsServicesApplication.class, args);
	}

}
