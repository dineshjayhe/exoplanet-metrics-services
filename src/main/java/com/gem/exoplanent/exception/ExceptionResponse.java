package com.gem.exoplanent.exception;

import lombok.Data;

@Data
public class ExceptionResponse {

    private String code;
    private String message;
    private String category;
}
