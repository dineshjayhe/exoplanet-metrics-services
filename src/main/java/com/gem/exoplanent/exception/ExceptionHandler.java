package com.gem.exoplanent.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
@Slf4j
public class ExceptionHandler {


    @org.springframework.web.bind.annotation.ExceptionHandler(Throwable.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ExceptionResponse processServerException(Throwable ex) {
        logError(ex, HttpStatus.INTERNAL_SERVER_ERROR);
        ExceptionResponse response = new ExceptionResponse();
        response.setCode(DefaultErrorCode.SERVICE_ERROR.getErrorCode());
        response.setMessage(DefaultErrorCode.SERVICE_ERROR.getMessage());
        response.setCategory(DefaultErrorCode.SERVICE_ERROR.getCategory());
        finalErrorLogger(response, HttpStatus.INTERNAL_SERVER_ERROR);
        return response;
    }


    protected void logError(final Throwable exception, final HttpStatus status) {
        log.error("exception: {}, exceptionMessage: {} status: {}", exception.getClass().getName(), exception.getMessage(), status);
        if (status.is5xxServerError()) {
            log.error("###Exception handled in exception handler: ", exception);
        }
    }

    protected void finalErrorLogger(ExceptionResponse response, HttpStatus status) {
        if (response != null) {
            log.error("##APP_ERROR || Error-Code: {}, Error-Message: {}, Http-Status-Code: {} || RG EXCEPTION HANDLER || END", response.getCode(), response.getMessage(), status.value());
        }
    }


    public enum DefaultErrorCode {
        INVALID_REQUEST("281401", "Invalid request format/data", "Common"),
        SERVICE_ERROR("281400", "Unable to process your request, please try again, if problem persist, contact system administrator", "Common");

        private final String errorCode;
        private final String message;
        private final String category;

        DefaultErrorCode(String errorCode, String message, String category) {
            this.errorCode = errorCode;
            this.message = message;
            this.category = category;
        }

        public String getErrorCode() {
            return errorCode;
        }

        public String getMessage() {
            return message;
        }

        public String getCategory() {
            return category;
        }
    }
}


