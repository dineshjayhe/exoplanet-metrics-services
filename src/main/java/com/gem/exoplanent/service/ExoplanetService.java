package com.gem.exoplanent.service;

import com.gem.exoplanent.client.ExoplanetClient;
import com.gem.exoplanent.modal.ExoplanetItem;
import com.gem.exoplanent.modal.Response;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Slf4j
public class ExoplanetService {

    private final ExoplanetClient exoplanetClient;

    public Response getMetricsOfExoplanet() {
        Response response = new Response();
        List<ExoplanetItem> exoplanetList = exoplanetClient.getExoplanetData();
        log.info("Total received size of exoplanet: {}", exoplanetList.size());
        long noOfOrphanPlanets = exoplanetList.stream().filter(item -> item.getTypeFlag() == 3).count();
        response.setNoOfOrphanPlanets(noOfOrphanPlanets);
        Map<String, Map<String, List<ExoplanetItem>>> expo = exoplanetList.stream().collect(Collectors.groupingBy(ExoplanetItem::getDiscoveryYear, TreeMap::new, Collectors.groupingBy(
                value -> {
                    if (value.getRadiusJpt() != null) {
                        if (value.getRadiusJpt() < 1) {
                            return "small";
                        } else if(value.getRadiusJpt() > 2) {
                            return "large";
                        } else {
                            return "medium";
                        }
                    } else {
                        return "noRadiusItems";
                    }
                }
        )));
        response.setGroupByYear(expo);
        exoplanetList.stream().max((current, other) -> {
            int currentStar = current.getHostStarTempK() == null ? 0 : current.getHostStarTempK();
            int otherStar = other.getHostStarTempK() == null ? 0 : other.getHostStarTempK();
            return Integer.compare(currentStar, otherStar);
        }).map(ExoplanetItem::getPlanetIdentifier).ifPresent(response::setPlanetOrbitingHottestStar);
        return response;
    }
}
